cities_api
==========

A Symfony project created on September 5, 2018, 7:31 pm.
# cities_api

- Unpack zip file in folder of your choice or run command ```git clone https://savicilija85@bitbucket.org/savicilija85/cities_api.git```
- Open command line or console and go to that folder and enter next command: ```composer install```
- When prompted, enter the database connection information, but in the field ```database_name``` enter ```cities_api```
- Enter command: ```php bin/console doctrine:database:create``` . This command will create database
- Enter command: ```php bin/console doctrine:schema:update --force``` . This command will create tables
- Enter command: ```php bin/console population:get {argument}``` . This command will populate database with 
100 entries if there is no argument, if there is an argument database will be populated with that number. 
Argument must be integer type.
- Enter command: ```php bin/console server:run``` .  This command will run Symfony built-in server
- Open a program that can send and receive HTTP requests, for example Postman
- There is a three routes:

| Method | Route | Explanation |
| ------ | ----- | ----------- |
| GET | /api/cities | Get all cities from database |
| GET | /api/cities/{id} | Get one city from database based on id parameter |
| DELETE | /api/cities/{id} | Delete one city from database based on id parameter |

- If you wish to delete database and dump database content to json file somewhere on disk run: 
```php bin/console population:clear {argument}```, if there is no argument file ```data.json``` will be saved to 
```/tmp``` directory. If argument exist file ```data.json``` will be saved in directory same as argument.
Argument must be in the following form: ```directory_name/subdirectory```


